﻿namespace Cmsl.Sdk.CSharp.Database.Abstractions;

/// <summary>
/// Represents objects that have a property Id from the specified type.
/// </summary>
/// <typeparam name="TId">The Id type</typeparam>
public interface IHaveReadonly<TId>
{
    /// <summary>
    /// Object's identifier.
    /// </summary>
    public TId Id { get; }
}