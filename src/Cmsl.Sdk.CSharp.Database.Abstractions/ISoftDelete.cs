﻿namespace Cmsl.Sdk.CSharp.Database.Abstractions;

/// <summary>
/// Represents an object that can be soft deleted, that is, although its records remain, it will not be shown in the regular searches.
/// </summary>
public interface ISoftDelete
{
    /// <summary>
    /// Gets the object's deletion state.
    /// </summary>
    public bool IsDeleted { get; }

    /// <summary>
    /// Soft deletes the object.
    /// </summary>
    public void Delete();

}