﻿namespace Cmsl.Sdk.CSharp.Extensions.IEnumerable;

public static class EnumerableExtensions
{
    public static bool Empty<T>(this IEnumerable<T> enumerable) => !enumerable.Any();
}