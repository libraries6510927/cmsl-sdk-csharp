﻿using System.Text.RegularExpressions;
using Cmsl.Sdk.CSharp.Validation.Abstractions;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Identification
{
    public record Email : IValidatable
    {
        public const string EmptyEmailErrorCode = nameof(EmptyEmailErrorCode);
        public const string InvalidEmailErrorCode = nameof(InvalidEmailErrorCode);
        
        public string Value { get; init; } = string.Empty;
        public Result Validate()
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value)) return Result.CreateInvalid(new[] {EmptyEmailErrorCode});

            const string emailRegex = @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"+
                @"@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

            if (!Regex.IsMatch(Value, emailRegex, RegexOptions.IgnoreCase)) return Result.CreateInvalid(new[] {InvalidEmailErrorCode});

            return Result.CreateSuccessful();
        }
    }
}