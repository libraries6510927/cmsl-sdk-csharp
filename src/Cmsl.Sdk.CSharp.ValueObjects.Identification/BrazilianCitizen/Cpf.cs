﻿using Cmsl.Sdk.CSharp.ValueObjects.Base.Text;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Identification.BrazilianCitizen
{
    public record Cpf : RequiredOnlyNumbersString
    {
        public const string InvalidCpfLengthErrorCode = nameof(InvalidCpfLengthErrorCode);
        public const string CpfConstitutedByEqualNumbersErrorCode = nameof(CpfConstitutedByEqualNumbersErrorCode);
        public const string InvalidCpfVerifyingDigitsErrorCode = nameof(InvalidCpfVerifyingDigitsErrorCode);
        
        public override Result Validate()
        {
            var initialValidation = base.Validate();

            return initialValidation.Match(x =>
            {
                const int cpfLength = 11;
                if (Value.Length != cpfLength) return Result.CreateInvalid(new[] { InvalidCpfLengthErrorCode });

                var firstNumber = Value.First();
                if (Value.All(y => y == firstNumber)) return Result.CreateInvalid(new[] { CpfConstitutedByEqualNumbersErrorCode });

                var numbers = GetAsNumberArray();
                var sum01 = GetCpfSum(10, numbers);
                var sum02 = GetCpfSum(11, numbers);

                if (!(CalculateVerifyingValue(sum01) == numbers[^2] && CalculateVerifyingValue(sum02) == numbers[^1]))
                    return Result.CreateInvalid( new[] { InvalidCpfVerifyingDigitsErrorCode });

                return Result.CreateSuccessful();
            }, x => x);
        }
        
        private static int GetCpfSum(int startWeight, int[] numbers)
        {
            var sum = 0;
            const int finalWeight = 2;
            for (var i = startWeight; i >= finalWeight; i--)
            {
                var index = startWeight - i;
                sum += numbers[index] * i;
            }

            return sum;
        }

        private static int CalculateVerifyingValue(int sum)
        {
            var verifyingValue = sum * 10 % 11;
            return verifyingValue == 10 ? 0 : verifyingValue;
        }


    }
    

}