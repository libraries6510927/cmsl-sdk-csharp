﻿using System.Text;
using Cmsl.Sdk.CSharp.ValueObjects.Base.Text;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Identification.Companies
{
    public record Cnpj : RequiredOnlyNumbersString
    {
        public const string InvalidCpnjLengthErrorCode = nameof(InvalidCpnjLengthErrorCode);
        public const string CnpjConstitutedByEqualNumbersErrorCode = nameof(CnpjConstitutedByEqualNumbersErrorCode);
        public const string InvalidCnpjVerifyingDigitsErrorCode = nameof(InvalidCnpjVerifyingDigitsErrorCode);
        
        public override Result Validate()
        {
            var initialValidation = base.Validate();

            return initialValidation.Match(x =>
            {
                const int cnpjLength = 14;
                if (Value.Length != cnpjLength) return Result.CreateInvalid(new[] { InvalidCpnjLengthErrorCode });

                var firstNumber = Value.First();
                if (Value.All(y => y == firstNumber)) return Result.CreateInvalid(new[] { CnpjConstitutedByEqualNumbersErrorCode });

                var numbers = GetAsNumberArray();
                if (numbers[^2] != GetVerifyingDigit01(numbers)) return Result.CreateInvalid(new[] { InvalidCnpjVerifyingDigitsErrorCode });
                if (numbers[^1] != GetVerifyingDigit02(numbers)) return Result.CreateInvalid(new[] { InvalidCnpjVerifyingDigitsErrorCode });

                return Result.CreateSuccessful();
            }, x => x);
        }

        private int GetVerifyingDigit01(int[] numbers)
        {
            var sum = GetCnpjSum(12, numbers);
            return GetVerifyingDigit(sum);
        }

        private int GetVerifyingDigit02(int[] numbers)
        {
            var sum = GetCnpjSum(13, numbers);
            return GetVerifyingDigit(sum);
        }

        private int GetVerifyingDigit(int sum)
        {
            var module = sum % 11;
            if (module < 2) return 0;
            return 11 - module;
        }

        private int GetCnpjSum(int value, int[] numbers)
        {
            var weights = value == 12 ? GetCnpjWeightFor12Numbers() : GetCnpjWeightFor13Numbers();
            var sum = 0;
            for (var i = 0; i < value; i++)
            {
                sum += weights[i] * numbers[i];
            }

            return sum;
        }

        private static int[] GetCnpjWeightFor12Numbers() => new[] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        private static int[] GetCnpjWeightFor13Numbers() => new[] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        
        public string GetFormattedValue()
        {
            var validation = Validate();
            return validation.Match(x =>
            {
                var builder = new StringBuilder();
                builder.Append(Value[..2]);
                builder.Append('.');
                builder.Append(Value[2..5]);
                builder.Append('.');
                builder.Append(Value[5..8]);
                builder.Append('/');
                builder.Append(Value[8..12]);
                builder.Append('-');
                builder.Append(Value[12..]);

                return builder.ToString();
            }, x => throw new InvalidOperationException($"Unable to format value if it is invalid! Ensure the value is valid first by calling .{nameof(Validate)}()."));
        }
    }
}
