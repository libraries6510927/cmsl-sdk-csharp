﻿using System.Collections.ObjectModel;

namespace Cmsl.Sdk.CSharp.WrapperTypes.Pagination;

public record Pagination<TResult>
{
    public IReadOnlyCollection<TResult> Items { get; init; } = new ReadOnlyCollection<TResult>(new List<TResult>());
    public int ReturnedItemsCount => Items.Count;
    public bool HasData => Items.Any();
    public int TotalItemsCount { get; init; }

    public int TotalPagesCount
    {
        get
        {
            var totalPages = TotalItemsCount / MaximumItemsPerPage;
            if (TotalItemsCount % MaximumItemsPerPage > 0) totalPages++;

            return totalPages;
        }
    }

    public int CurrentPage { get; init; }
    public int MaximumItemsPerPage { get; init; }

    

}