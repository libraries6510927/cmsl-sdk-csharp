﻿using Cmsl.Sdk.CSharp.Validation.Abstractions;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.WrapperTypes.Pagination;

public record PaginationRequest : IValidatable
{
    public const string InvalidPaginationRequestErrorCode = nameof(InvalidPaginationRequestErrorCode);
    
    public int ItemsPerPage { get; set; } = 0;
    public int Page { get; set; } = 0;
    public Result.Result Validate()
    {
        if(ItemsPerPage <= 0 || Page <= 0) return Result.Result.CreateInvalid(new[] {InvalidPaginationRequestErrorCode});
        return Result.Result.CreateSuccessful();
    }
    
    
}