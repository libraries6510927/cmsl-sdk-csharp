﻿using Cmsl.Sdk.CSharp.Comparison.Abstractions;

namespace Cmsl.Sdk.CSharp.Comparison.Extensions.IEnumerable;

public static class EnumerableExtensions
{
    public static bool SequenceEquivalent<T1, T2>(this IEnumerable<T1> x, IEnumerable<T2> y, IEqualityComparer<T1, T2> comparer)
    {
        return x.Count() == y.Count()
               && x.All(a => y.Any(b => comparer.Equals(a, b)));
    }
    
    public static bool SequenceEquivalent<T>(this IEnumerable<T> x, IEnumerable<T> y, IEqualityComparer<T> comparer)
    {
        return x.Count() == y.Count()
               && x.All(a => y.Contains(a, comparer));
    }

    public static bool SequenceEquivalent<T>(this IEnumerable<T> x, IEnumerable<T> y) => SequenceEquivalent(x, y, EqualityComparer<T>.Default);
}