﻿namespace Cmsl.Sdk.CSharp.Extensions.Assembly;

public static class AssemblyExtensions
{
    public static string GetDirectoryName(this System.Reflection.Assembly assembly)
    {
        var location = assembly.Location;
        var directoryName = Path.GetDirectoryName(location);
        if (string.IsNullOrEmpty(directoryName)) throw new InvalidOperationException("Unable to get assembly directory name");

        return directoryName;
    }
}