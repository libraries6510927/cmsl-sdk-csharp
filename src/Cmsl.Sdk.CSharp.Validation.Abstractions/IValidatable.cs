﻿using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.Validation.Abstractions
{
    /// <summary>
    /// Represents an object that can be Validated.
    /// </summary>
    public interface IValidatable
    {
        /// <summary>
        /// Validates this object.
        /// </summary>
        /// <returns>An instance of Result</returns>
        Result Validate();
    }
}