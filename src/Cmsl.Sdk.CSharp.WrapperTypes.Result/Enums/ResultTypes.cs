﻿namespace Cmsl.Sdk.CSharp.WrapperTypes.Result.Enums;

public enum ResultTypes
{
    Success,
    Invalid,
    NotFound,
    Unauthorized,
    Error
}