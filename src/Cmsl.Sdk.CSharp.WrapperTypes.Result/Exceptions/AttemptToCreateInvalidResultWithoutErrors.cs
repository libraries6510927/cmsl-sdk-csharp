﻿namespace Cmsl.Sdk.CSharp.WrapperTypes.Result.Exceptions;

public class AttemptToCreateInvalidResultWithoutErrors: Exception
{
    private const string CustomMessage = "Unable to create invalid result withou any error";

    public AttemptToCreateInvalidResultWithoutErrors(Exception? innerException = null) : base(CustomMessage, innerException)
    {
    }
}