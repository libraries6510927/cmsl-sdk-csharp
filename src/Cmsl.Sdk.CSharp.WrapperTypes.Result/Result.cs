﻿using Cmsl.Sdk.CSharp.Extensions.IEnumerable;
using Cmsl.Sdk.CSharp.WrapperTypes.Result.Enums;
using Cmsl.Sdk.CSharp.WrapperTypes.Result.Exceptions;

namespace Cmsl.Sdk.CSharp.WrapperTypes.Result;

public record Result
{
    /// <summary>
    /// The error list.
    /// </summary>
    public IEnumerable<string> Errors { get; init; } = new List<string>();
    public bool IsSuccess => Type == ResultTypes.Success &&  !Errors.Any();
    public ResultTypes Type { get; init; } = ResultTypes.Success;

    /// <summary>
    /// Perform actions with the result data.
    /// </summary>
    /// <param name="onSuccess">Callback to execute when the result is successful.</param>
    /// <param name="onFailure">Callback to execute when the result is failure.</param>
    /// <typeparam name="TReturn">The return type.</typeparam>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">When at least one of the callbacks are null.</exception>
    public TReturn Match<TReturn>(Func<Result, TReturn> onSuccess, Func<Result, TReturn> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        return IsSuccess ? onSuccess.Invoke(this) : onFailure.Invoke(this);
    }

    /// <summary>
    /// Perform actions with the result data.
    /// </summary>
    /// <param name="onSuccess">Callback to execute when the result is successful.</param>
    /// <param name="onFailure">Callback to execute when the result is failure.</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">When at least one of the callbacks are null.</exception>
    public void Match(Action<Result> onSuccess, Action<Result> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        if (IsSuccess) onSuccess.Invoke(this);
        else onFailure.Invoke(this);
    }

    public Task MatchAsync(Func<Result, Task> onSuccess, Func<Result, Task> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        return IsSuccess ? onSuccess.Invoke(this) : onFailure.Invoke(this);
    }
    
    public Task<TReturn> MatchAsync<TReturn>(Func<Result, Task<TReturn>> onSuccess, Func<Result, Task<TReturn>> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        return IsSuccess ? onSuccess.Invoke(this) : onFailure.Invoke(this);
    }

    public static Result CreateSuccessful() => new()
    {
        Type = ResultTypes.Success
    };
    
    public static Result CreateInvalid(IEnumerable<string> errors)
    {
        if (errors.Empty()) throw new AttemptToCreateInvalidResultWithoutErrors();

        return new Result
        {
            Errors = errors,
            Type = ResultTypes.Invalid
        };
    }

    public static Result CreateNotFound(IEnumerable<string>? errors = null)
    {
        return new Result
        {
            Errors = errors ?? Array.Empty<string>(),
            Type = ResultTypes.NotFound
        };
    }
    
    public static Result CreateUnauthorized(IEnumerable<string>? errors = null)
    {
        return new Result
        {
            Errors = errors ?? Array.Empty<string>(),
            Type = ResultTypes.Unauthorized
        };
    }
    
    public static Result CreateError(IEnumerable<string>? errors = null)
    {
        return new Result
        {
            Errors = errors ?? Array.Empty<string>(),
            Type = ResultTypes.Error
        };
    }
}

public record Result<TData> : Result
{
    /// <summary>
    /// The result data.
    /// </summary>
    public TData? Data { get; init; }

    /// <summary>
    /// Perform actions with the result data.
    /// </summary>
    /// <param name="onSuccess">Callback to execute when the result is successful.</param>
    /// <param name="onFailure">Callback to execute when the result is failure.</param>
    /// <typeparam name="TReturn">The return type.</typeparam>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">When at least one of the callbacks are null.</exception>
    public TReturn Match<TReturn>(Func<Result<TData>, TReturn> onSuccess, Func<Result<TData>, TReturn> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        return IsSuccess && Data is not null ? onSuccess.Invoke(this) : onFailure.Invoke(this);
    }
        
    /// <summary>
    /// Perform actions with the result data.
    /// </summary>
    /// <param name="onSuccess">Callback to execute when the result is successful.</param>
    /// <param name="onFailure">Callback to execute when the result is failure.</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">When at least one of the callbacks are null.</exception>
    public void Match(Action<Result<TData>> onSuccess, Action<Result<TData>> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        if (IsSuccess) onSuccess.Invoke(this);
        else onFailure.Invoke(this);
    }
    
    public Task MatchAsync(Func<Result<TData>, Task> onSuccess, Func<Result<TData>, Task> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        return IsSuccess ? onSuccess.Invoke(this) : onFailure.Invoke(this);
    }
    
    public Task<TResult> MatchAsync<TResult>(Func<Result<TData>, Task<TResult>> onSuccess, Func<Result<TData>, Task<TResult>> onFailure)
    {
        if (onSuccess is null) throw new ArgumentNullException(nameof(onSuccess));
        if (onFailure is null) throw new ArgumentNullException(nameof(onFailure));
        
        return IsSuccess ? onSuccess.Invoke(this) : onFailure.Invoke(this);
    }

    public static Result<TData> CreateSuccessful(TData data)
    {
        if (data is null) throw new ArgumentNullException(nameof(data));

        return new Result<TData>
        {
            Data = data,
            Type = ResultTypes.Success
        };
    }
    
    public new static Result<TData> CreateInvalid(IEnumerable<string> errors)
    {
        if (errors.Empty()) throw new AttemptToCreateInvalidResultWithoutErrors();

        return new Result<TData>
        {
            Errors = errors,
            Type = ResultTypes.Invalid
        };
    }

    public new static Result<TData> CreateNotFound(IEnumerable<string>? errors = null)
    {
        return new Result<TData>
        {
            Errors = errors ?? Array.Empty<string>(),
            Type = ResultTypes.NotFound
        };
    }
    
    public new static Result<TData> CreateUnauthorized(IEnumerable<string>? errors = null)
    {
        return new Result<TData>
        {
            Errors = errors ?? Array.Empty<string>(),
            Type = ResultTypes.Unauthorized
        };
    }
    
    public new static Result<TData> CreateError(IEnumerable<string>? errors = null)
    {
        return new Result<TData>
        {
            Errors = errors ?? Array.Empty<string>(),
            Type = ResultTypes.Error
        };
    }

    public static Result<TData> CreateFromUnsuccessfulResult(Result result, TData? data)
    {
        if (result.IsSuccess) throw new InvalidOperationException("The result should be unsuccessful");

        return new Result<TData>
        {
            Type = result.Type,
            Errors = result.Errors,
            Data = data
        };
    }
    
    public static Result<TData> CreateFromUnsuccessfulResult(Result result)
    {
        if (result.IsSuccess) throw new InvalidOperationException("The result should be unsuccessful");

        return new Result<TData>
        {
            Type = result.Type,
            Errors = result.Errors,
        };
    }
}