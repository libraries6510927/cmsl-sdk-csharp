﻿using Cmsl.Sdk.CSharp.Validation.Abstractions;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Numeric;

public record UnsignedDecimal : IValidatable
{
    public const string NegativeValueErrorCode = nameof(NegativeValueErrorCode);
    
    public decimal Value { get; init; }

    public static UnsignedDecimal operator +(UnsignedDecimal a, UnsignedDecimal b) => new() { Value = a.Value + b.Value };
    public static UnsignedDecimal operator +(int a, UnsignedDecimal b) => new() { Value = a + b.Value };
    public static UnsignedDecimal operator +(UnsignedDecimal a, int b) => new() { Value = a.Value + b };

    public static UnsignedDecimal operator -(UnsignedDecimal a, UnsignedDecimal b) => new() { Value = a.Value - b.Value };
    
    public static UnsignedDecimal operator *(int a, UnsignedDecimal b) => new () {Value = a * b.Value};

    public static implicit operator UnsignedDecimal(decimal a) => new() { Value = a };

    public virtual Result Validate()
    {
        return Value < 0 ? Result.CreateInvalid(new[] { NegativeValueErrorCode}) : Result.CreateSuccessful();
    }
}