﻿using Cmsl.Sdk.CSharp.Validation.Abstractions;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Numeric;

public record NonZeroPositiveInt : IValidatable
{
    public const string ZeroOrNegativeValueErrorCode = nameof(ZeroOrNegativeValueErrorCode);

    public int Value { get; init; }

    public Result Validate() => Value <= 0
        ? Result.CreateInvalid(new[] { ZeroOrNegativeValueErrorCode })
        : Result.CreateSuccessful();

    public static implicit operator NonZeroPositiveInt(int a) => new() { Value = a };
    public static explicit operator int(NonZeroPositiveInt a) => a.Value;

    public static int operator -(NonZeroPositiveInt a, int b) => a.Value - b;
    public static int operator -(int a, NonZeroPositiveInt b) => a - b.Value;
    public static int operator *(NonZeroPositiveInt a, int b) => a.Value * b;
    public static int operator *(int a, NonZeroPositiveInt b) => a * b.Value;

    public override string ToString() => Value.ToString();
}