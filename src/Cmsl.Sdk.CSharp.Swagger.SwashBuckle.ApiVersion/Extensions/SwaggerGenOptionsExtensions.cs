﻿using Cmsl.Sdk.CSharp.Swagger.SwashBuckle.ApiVersion.Filters.DocumentFilters;
using Cmsl.Sdk.CSharp.Swagger.SwashBuckle.ApiVersion.Filters.OperationFilters;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Cmsl.Sdk.CSharp.Swagger.SwashBuckle.ApiVersion.Extensions;

public static class SwaggerGenOptionsExtensions
{
    /// <summary>
    /// Removes the api version from swagger parameter
    /// </summary>
    /// <param name="swaggerGenOptions">The swagger generator options.</param>
    /// <param name="apiVersionParameterName">The parameter name. Eg: "apiVersion" </param>
    /// <param name="apiVersionRouteParameter">The parameter description in the route. Eg: "v{apiVersion}"</param>
    /// <returns></returns>
    public static SwaggerGenOptions RemoveApiVersionFromSwaggerParameters(this SwaggerGenOptions swaggerGenOptions, string apiVersionParameterName, string apiVersionRouteParameter)
    {
        swaggerGenOptions.OperationFilter<RemoveVersionParameterFilter>(apiVersionParameterName);
        swaggerGenOptions.DocumentFilter<ReplaceVersionParameterWithCurrentVersionFilter>(apiVersionRouteParameter);

        return swaggerGenOptions;
    }
}