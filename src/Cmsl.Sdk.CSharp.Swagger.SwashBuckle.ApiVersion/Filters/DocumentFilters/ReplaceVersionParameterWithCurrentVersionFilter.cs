﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Cmsl.Sdk.CSharp.Swagger.SwashBuckle.ApiVersion.Filters.DocumentFilters;

internal class ReplaceVersionParameterWithCurrentVersionFilter : IDocumentFilter
{
    private readonly string _apiVersionRouteParameter;

    public ReplaceVersionParameterWithCurrentVersionFilter(string apiVersionRouteParameter)
    {
        _apiVersionRouteParameter = apiVersionRouteParameter;
    }

    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var newPaths  = swaggerDoc.Paths
            .ToDictionary(x => x.Key.Replace(_apiVersionRouteParameter, swaggerDoc.Info.Version),
                y=> y.Value);

        swaggerDoc.Paths = new OpenApiPaths();

        foreach (var path in newPaths)
        {
            swaggerDoc.Paths[path.Key] = path.Value;
        }
    }
}