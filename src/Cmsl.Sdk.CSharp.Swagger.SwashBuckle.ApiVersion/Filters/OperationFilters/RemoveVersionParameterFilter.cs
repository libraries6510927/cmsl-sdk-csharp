﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Cmsl.Sdk.CSharp.Swagger.SwashBuckle.ApiVersion.Filters.OperationFilters;

internal class RemoveVersionParameterFilter : IOperationFilter
{
    private readonly string _apiVersionParameterName;

    public RemoveVersionParameterFilter(string apiVersionParameterName)
    {
        _apiVersionParameterName = apiVersionParameterName;
    }

    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        var parameter = operation.Parameters.First(x => x.Name == _apiVersionParameterName);
        operation.Parameters.Remove(parameter);
    }
}