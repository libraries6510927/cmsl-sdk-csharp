﻿using Cmsl.Sdk.CSharp.Database.EFCore.ValueObjects.Converters.ValueConverters;
using Cmsl.Sdk.CSharp.ValueObjects.Base.Text;
using Cmsl.Sdk.CSharp.ValueObjects.Numeric;
using Microsoft.EntityFrameworkCore;

namespace Cmsl.Sdk.CSharp.Database.EFCore.ValueObjects.Converters.Extensions;

public static class ModelConfigurationBuilderExtensions
{
    /// <summary>
    /// Apply the value converter that treats NonEmptyString as default string in the database.
    /// </summary>
    /// <param name="configurationBuilder">The configuration builder.</param>
    /// <returns>The instance of ModelConfigurationBuilder</returns>
    public static ModelConfigurationBuilder UseNonEmptyStringAsString(this ModelConfigurationBuilder configurationBuilder)
    {
        configurationBuilder
            .Properties<NonEmptyString>()
            .HaveConversion<NonEmptyStringToStringValueConverter>();

        return configurationBuilder;
    }

    /// <summary>
    /// Apply the value converter that treats NonZeroPositiveInt as default int in the database.
    /// </summary>
    /// <param name="configurationBuilder">The configuration builder.</param>
    /// <returns>The instance of ModelConfigurationBuilder</returns>
    public static ModelConfigurationBuilder UseNonZeroPositiveIntAsInt(this ModelConfigurationBuilder configurationBuilder)
    {
        configurationBuilder
            .Properties<NonZeroPositiveInt>()
            .HaveConversion<NonZeroPositiveIntToIntValueConverter>();

        return configurationBuilder;
    }

    /// <summary>
    /// Apply the value converter that treats UnsignedDecimal as default decimal in the database.
    /// </summary>
    /// <param name="configurationBuilder">The configuration builder.</param>
    /// <returns>The instance of ModelConfigurationBuilder</returns>
    public static ModelConfigurationBuilder UseUnsignedDecimalAsDecimal(this ModelConfigurationBuilder configurationBuilder)
    {
        configurationBuilder
            .Properties<UnsignedDecimal>()
            .HaveConversion<UnsignedDecimalConverterToDecimalValueConverter>();

        return configurationBuilder;
    }

    /// <summary>
    /// Apply the value converter that treats UnsignedDecimal as default decimal in the database with specified precision.
    /// </summary>
    /// <param name="configurationBuilder">The configuration builder.</param>
    /// <param name="precision">The total number of digits to be stored, including decimal and non decimal.</param>
    /// <param name="scale">The total number of decimal digits to be stored.</param>
    /// <returns>The instance of ModelConfigurationBuilder</returns>
    public static ModelConfigurationBuilder UseUnsignedDecimalAsDecimal(this ModelConfigurationBuilder configurationBuilder, int precision, int scale)
    {
        configurationBuilder
            .Properties<UnsignedDecimal>()
            .HaveConversion<UnsignedDecimalConverterToDecimalValueConverter>()
            .HavePrecision(precision, scale);

        return configurationBuilder;
    }

}