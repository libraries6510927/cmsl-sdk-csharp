﻿using System.Linq.Expressions;
using Cmsl.Sdk.CSharp.ValueObjects.Base.Text;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cmsl.Sdk.CSharp.Database.EFCore.ValueObjects.Converters.ValueConverters;

public class NonEmptyStringToStringValueConverter: ValueConverter<NonEmptyString, string>
{
    private static readonly Expression<Func<NonEmptyString, string>> ConvertToString = x => x.Value;
    private static readonly Expression<Func<string, NonEmptyString>> ConvertFromString = x => x;

    public NonEmptyStringToStringValueConverter() : base(ConvertToString, ConvertFromString)
    {
        
    }

}