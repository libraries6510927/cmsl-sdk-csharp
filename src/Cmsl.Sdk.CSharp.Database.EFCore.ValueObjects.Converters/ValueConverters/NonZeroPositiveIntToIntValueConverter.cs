﻿using System.Linq.Expressions;
using Cmsl.Sdk.CSharp.ValueObjects.Numeric;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cmsl.Sdk.CSharp.Database.EFCore.ValueObjects.Converters.ValueConverters;

public class NonZeroPositiveIntToIntValueConverter : ValueConverter<NonZeroPositiveInt, int>
{
    private static readonly Expression<Func<NonZeroPositiveInt, int>> ToInt = x => x.Value;
    private static readonly Expression<Func<int, NonZeroPositiveInt>> FromInt = x => x;

    public NonZeroPositiveIntToIntValueConverter() : base(ToInt, FromInt)
    {
    }
}