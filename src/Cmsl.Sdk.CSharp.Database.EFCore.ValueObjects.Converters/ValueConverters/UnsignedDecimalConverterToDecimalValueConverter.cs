﻿using System.Linq.Expressions;
using Cmsl.Sdk.CSharp.ValueObjects.Numeric;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Cmsl.Sdk.CSharp.Database.EFCore.ValueObjects.Converters.ValueConverters;

public class UnsignedDecimalConverterToDecimalValueConverter : ValueConverter<UnsignedDecimal, decimal>
{
    private static readonly Expression<Func<UnsignedDecimal, decimal>> UnsignedDecimalToDecimal = x => x.Value;
    private static readonly Expression<Func<decimal, UnsignedDecimal>> DecimalToUnsignedDecimal = x => x;

    public UnsignedDecimalConverterToDecimalValueConverter() : base(UnsignedDecimalToDecimal, DecimalToUnsignedDecimal)
    {
    }
}