﻿using System.Text.RegularExpressions;
using Cmsl.Sdk.CSharp.Validation.Abstractions;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Base.Text
{
    public record RequiredOnlyNumbersString : IValidatable
    {
        public const string NotOnlyNumbersErrorCode = nameof(NotOnlyNumbersErrorCode);
        public const string EmptyNumericStringErrorCode = nameof(EmptyNumericStringErrorCode);

        public string Value { get; init; } = string.Empty;
        
        protected bool IsOnlyNumbers() => Regex.IsMatch(Value, "^[0-9]*$");
        protected int[] GetAsNumberArray() => Value.Select(c => int.Parse(c.ToString())).ToArray();

        public virtual Result Validate()
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value)) return Result.CreateInvalid(new[] {EmptyNumericStringErrorCode});
            if (!IsOnlyNumbers()) return Result.CreateInvalid(new[] { NotOnlyNumbersErrorCode });

            return Result.CreateSuccessful();
        }
    }
}