﻿using Cmsl.Sdk.CSharp.Validation.Abstractions;
using Cmsl.Sdk.CSharp.WrapperTypes.Result;

namespace Cmsl.Sdk.CSharp.ValueObjects.Base.Text;

public record NonEmptyString : IValidatable
{
    public const string EmptyStringErrorCode = nameof(EmptyStringErrorCode);
    
    public string Value { get; init; } = string.Empty;

    public Result Validate() => IsEmpty() ? Result.CreateInvalid(new[] { EmptyStringErrorCode }) : Result.CreateSuccessful();
    public bool IsEmpty() => string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value);

    public static implicit operator string(NonEmptyString a) => a.Value;
    public static implicit operator NonEmptyString(string a) => new() { Value = a };
}