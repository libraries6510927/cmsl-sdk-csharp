# CMSL.Patterns

## Table of Contents

1. [Introduction](#introduction)
2. [Projects](#projects)
   1. [Patterns.Abstractions](#abstractions)
      1. [Interfaces](#abstractions.interfaces)
      2. [Types](#abstractions.types)
   2. [Patterns.ValueObjects](#valueObjects)
   3. [Patterns.DesignPatterns.ChainOfResponsibility](#designPatterns.cor)
   4. [Patterns.DesignPatterns.Cqrs](#designPatterns.cqrs)
   5. [Patterns.Testing.Asserts](#testingAsserts)


<div id="introduction"></div>

## Introduction
This repository contains the common patterns used in my projects.  Since this repository contains many projects, each section below describes one of them.

<div id="projects"></div>

## Projects
This section contains a short description of each project:

* Patterns.Abstractions: a package that has all interfaces, types, and abstractions
  used by the other ones;
* Patterns.ValueObjects: this project contains classes that represent some kind
  of information from the real world, including its validation logic;
* Patterns.DesignPatterns.ChainOfReponsibility: contains the abstractions and classes to implement the chain of responsibility pattern;
* Patterns.Testing.Asserts: this package contains custom assertions to be used in
  XUnit test projects.

<div id="abstractions"></div>

### Patterns.Abstractions
This project is divided into two parts: Interfaces and Types. The first one contains
all the interfaces used in the other projects. The second, on the other hand, has the common types
used as returns or parameters

<div id="abstractions.interfaces"></div>

#### Interfaces
* IValidatable: This interface exposes a method called Validate, which returns an instance of Result;
* IHaveId: The objects that implement this interface expose a property called Id of the specified generic type;
* ISoftDelete: This interface represents objects that can be soft deleted. Therefore, it exposes a property IsDeleted and a method to logically delete the object.

<div id="abstractions.types"></div>

#### Types
* Result: This type contains an IEnumerable of errors (string) and a set of "Match" methods to perform
  different actions in case of success and failure.
* PaginationRequest: this type is used to send pagination requests;
* Pagination: the response from a pagination request.

<div id="valueObjects"></div>

### Patterns.ValueObjects
This package contains objects that represent some kind of information from the real world.
Therefore, they also contain their validation logic by implementing IValidatable.

The subsections below show the value objects and their error codes, which are
strings that will be inserted into the Errors property from the Result type.

### Base Text Value Objects

#### Non Empty String
Represents a string that does not accept empty, white space, or null value.

| Error Code           | Explanation                              |
|----------------------|------------------------------------------|
| EmptyStringErrorCode | The value is empty, null, or white space |

### Base Numeric Value Objects

#### Required Only Numbers String

Represents a string that accepts only numbers.

| Error Code                  | Explanation                                                |
|-----------------------------|------------------------------------------------------------|
| EmptyNumericStringErrorCode | The string is empty, null, or is white space               |
| NotOnlyNumbersErrorCode     | The string provided contains other characters than numbers |

#### Unsigned Decimal

Contains zero or positive decimal numbers.

| Error Code             | Explanation           |
|------------------------|-----------------------|
| NegativeValueErrorCode | The value is negative |

#### Non Zero Positive Integer

Represents positive integers that can not be zero.

| Error Code                   | Explanation                   |
|------------------------------|-------------------------------|
| ZeroOrNegativeValueErrorCode | The value is negative or zero |

### People Value Objects

#### Email

| Error Code            | Explanation                                  |
|-----------------------|----------------------------------------------|
| EmptyEmailErrorCode   | The email address is empty.                  |
| InvalidEmailErrorCode | The email doesn't match the validation regex |

#### CPF

| Error Code                            | Explanation                                                                 |
|---------------------------------------|-----------------------------------------------------------------------------|
| InvalidCpfLengthErrorCode             | The CPF has a length different from 11                                      |
| CpfConstitutedByEqualNumbersErrorCode | The CPF is constituted by a string with equal characters. e.g.: 00000000000 |
| InvalidCpfVerifyingDigitsErrorCode    | The CPF's last two digits are incorrect                                     |

#### CNPJ

| Error Code                             | Explanation                                                                     |
|----------------------------------------|---------------------------------------------------------------------------------|
| InvalidCpnjLengthErrorCode             | The CNPJ has a length different from 14                                         |
| CnpjConstitutedByEqualNumbersErrorCode | The CNPJ is constituted by a string with equal characters. e.g.: 00000000000000 |
| InvalidCnpjVerifyingDigitsErrorCode    | The CNPJ's last two digits are incorrect                                        |

<div id="designPatterns.cor"></div>

### Patterns.DesignPatterns.ChainOfResponsibility

This project implements the Chain of Responsibility pattern. One can use it by inserting IChainElements into the IChainBuilder. After the use, all values stored by the process remain in the returned context to be further used.

<div id="designPatterns.cqrs"></div>

### Patterns.DesignPatterns.Cqrs

This package exposes interfaces to implement commands and queries. There are also the CqrsBase classes that use the Chain of Responsibility pattern to compose the actions.

<div id="testingAsserts"></div>

### Patterns.Testing.Asserts

As previously said, this project contains custom assertions that can be used in
xUnit test projects. The subsections below show each one of them.

#### List Assertions (ListAsserts class)

* SameElements: ensures that the two IEnumerable provided have the same length and that all the elements are equal,
  regardless of the order.
